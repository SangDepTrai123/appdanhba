package com.example.contactapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.SearchView;

import com.example.contactapp.Model.Contact;
import com.example.contactapp.Model.ContactAdapter;
import com.example.contactapp.Model.RoomDatabase;
import com.example.contactapp.databinding.ActivityMainBinding;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private RecyclerView rv_contact;
    private FloatingActionButton flb_addcontact;
    private ArrayList<Contact> contactArrayList;
    private RoomDatabase database;
    private ContactAdapter adapter;
    private ActivityMainBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View viewroot = binding.getRoot();
        setContentView(viewroot);
        getData();
        setComponent();
        setEvents();
    }
    void getData()
    {
        contactArrayList=new ArrayList<Contact>();
        database= RoomDatabase.getInstance(MainActivity.this);
        contactArrayList=(ArrayList<Contact>) database.contactDAO().getAll();
    }
    void setComponent()
    {
        rv_contact=binding.rvContact;
        adapter=new ContactAdapter(contactArrayList,MainActivity.this);
        rv_contact.setLayoutManager(new LinearLayoutManager(this));
        rv_contact.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        flb_addcontact=binding.flbAddcontact;
    }
    void setEvents()
    {
        flb_addcontact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,NewContactActivity.class));
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu,menu);
        MenuItem item=menu.findItem(R.id.action_search);
        SearchView searchView=(SearchView) item.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                adapter.getFilter().filter(s);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }
}