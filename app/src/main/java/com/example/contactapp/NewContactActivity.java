package com.example.contactapp;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.contactapp.Model.Contact;
import com.example.contactapp.Model.RoomDatabase;
import com.example.contactapp.databinding.ActivityNewContactBinding;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.normal.TedPermission;

import java.io.IOException;
import java.util.List;

import gun0912.tedbottompicker.TedBottomPicker;
import gun0912.tedbottompicker.TedBottomSheetDialogFragment;

public class NewContactActivity extends AppCompatActivity {
    private EditText et_name,et_number,et_email;
    private FloatingActionButton btn_save,btn_chooseImage;
    private ImageView thumbnail;
    private Uri avt_uri;
    private ActivityNewContactBinding binding;
    private RoomDatabase database;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding=ActivityNewContactBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        setComponents();
        getData();
        setEvents();
    }
    private void setComponents()
    {
        et_name=binding.edName;
        et_number=binding.edNumber;
        et_email=binding.edEmail;
        thumbnail=binding.thumbnail;
        btn_save=binding.flbSavecontact;
        btn_chooseImage=binding.flbChooseImage;
    }
    private void getData()
    {
        Bundle bundle =getIntent().getExtras();
        if(bundle==null)
        {
            return;
        }
        Contact contact= (Contact) bundle.get("object_contact");
        et_number.setText(contact.getNumber());
        et_name.setText(contact.getName());
        et_email.setText(contact.getEmail());
        thumbnail.setImageURI(Uri.parse(contact.getUri()));
    }
    private Contact getContact()
    {
        String name=et_name.getText().toString();
        String number=et_number.getText().toString();
        String email=et_email.getText().toString();
        return new Contact(number,name,email,avt_uri.toString());
    }
    private void setEvents()
    {
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Contact contact=getContact();
                database =RoomDatabase.getInstance(NewContactActivity.this);
                database.contactDAO().insertAll(contact);
                startActivity(new Intent(NewContactActivity.this,MainActivity.class));
            }
        });
        btn_chooseImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestPermission();
            }
        });
    }
    private void requestPermission()
    {
        PermissionListener permissionlistener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                Toast.makeText(NewContactActivity.this, "Permission Granted", Toast.LENGTH_SHORT).show();
                openImagePicker();
            }

            @Override
            public void onPermissionDenied(List<String> deniedPermissions) {
                Toast.makeText(NewContactActivity.this, "Permission Denied\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
            }

        };
        TedPermission.create()
                .setPermissionListener(permissionlistener)
                .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                .setPermissions(Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .check();
    }
    private void openImagePicker()
    {
        TedBottomPicker.with(NewContactActivity.this)
                .show(new TedBottomSheetDialogFragment.OnImageSelectedListener() {
                    @Override
                    public void onImageSelected(Uri uri) {
                        // here is selected image uri
                        Bitmap bitmap=null;
                        avt_uri=uri;
                        try {
                            bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(),uri);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        thumbnail.setImageBitmap(bitmap);
                    }
                });
    }
}