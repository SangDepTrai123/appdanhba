package com.example.contactapp.Model;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.contactapp.NewContactActivity;
import com.example.contactapp.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ViewHolder> implements Filterable {
    List<Contact> list_input;
    List<Contact> list_output;
    Context context;
    RoomDatabase database;
    public ContactAdapter(List<Contact> contactList, Context context) {
        this.list_input = contactList;
        this.list_output = contactList;
        this.context = context;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ContactAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemview = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_card,parent,false);
        return new ViewHolder(itemview);
    }

    @Override
    public void onBindViewHolder(@NonNull ContactAdapter.ViewHolder holder, int position) {
           holder.tv_number.setText(list_output.get(position).getNumber());
           holder.tv_name.setText(list_output.get(position).getName());
           holder.img_avt.setImageURI(Uri.parse(list_output.get(position).getUri()));
           int p=position;
           holder.layout.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View view) {
                   OnClickGotoDetail(list_output.get(p));
               }
           });
           holder.flb_delete.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View view) {
                   buildDialog(p);
               }
           });
    }
    private void buildDialog(int position)
    {
        AlertDialog.Builder b = new AlertDialog.Builder(context);
        b.setMessage("Xoa contact nay?");
        b.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                database=RoomDatabase.getInstance(context);
                database.contactDAO().delete(list_output.get(position));
                list_output.remove(position);
                notifyDataSetChanged();
            }
        });
        b.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        AlertDialog alertDialog=b.create();
        alertDialog.show();
    }
    private void OnClickGotoDetail(Contact contact)
    {
        Intent intent=new Intent(context, NewContactActivity.class);
        Bundle bundle=new Bundle();
        bundle.putSerializable("object_contact",contact);
        intent.putExtras(bundle);
        context.startActivity(intent);
    }
    @Override
    public int getItemCount() {
        return list_output.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                List<Contact> listFiltered=new ArrayList<>();
                if(charString.isEmpty())
                {
                    listFiltered.addAll(list_input);
                }
                else {
                    for(Contact c : list_input)
                    {
                        if(c.getName().toLowerCase().contains(charString.toLowerCase())||c.getNumber().toLowerCase().contains(charString.toLowerCase()))
                        {
                            listFiltered.add(c);
                        }
                    }
                }
                FilterResults results=new FilterResults();
                results.values=listFiltered;
                return results;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                list_output=(ArrayList<Contact>)filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_name,tv_number;
        private ImageView img_avt;
        private FloatingActionButton flb_delete;
        private ConstraintLayout layout;
        public ViewHolder(View view) {
            super(view);
            tv_name=view.findViewById(R.id.tv_name);
            tv_number=view.findViewById(R.id.tv_number);
            img_avt=view.findViewById(R.id.img_avt);
            flb_delete=view.findViewById(R.id.flb_delete);
            layout=view.findViewById(R.id.item_layout);
        }
    }
}
