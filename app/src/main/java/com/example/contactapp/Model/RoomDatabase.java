package com.example.contactapp.Model;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;

@Database(entities = {Contact.class}, version = 2,exportSchema = false)
public abstract class RoomDatabase extends androidx.room.RoomDatabase {
    public abstract ContactDAO contactDAO();
    private static RoomDatabase database;
    private static String DATABASE_NAME="database";
    public synchronized static RoomDatabase getInstance(Context context)
    {
        if(database==null)
        {
            database= Room.databaseBuilder(context.getApplicationContext(),RoomDatabase.class,DATABASE_NAME)
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return database;
    }
}
