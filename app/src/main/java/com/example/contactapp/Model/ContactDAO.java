package com.example.contactapp.Model;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface ContactDAO {
    @Query("SELECT * FROM Contact")
    List<Contact> getAll();
    @Insert
    void insertAll(Contact... contacts);
    @Delete
    void delete(Contact contact);
    @Query("SELECT * FROM Contact WHERE name = :sname  LIMIT 1")
    public List<Contact> findByName(String sname);
    @Update
    public void updateContacts(Contact... contacts);
    @Query("SELECT * FROM Contact WHERE ID= :id LIMIT 1 " )
    public Contact findContactWithID(int id);
}
